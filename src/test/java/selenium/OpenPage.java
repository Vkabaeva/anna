package selenium;

import org.openqa.selenium.*;

import static com.codeborne.selenide.Selenide.$;

public class OpenPage {

    public Double usdSalaryDouble() {
        String usdSalaryS = $(By.xpath("(//*[contains(@class,'main-page-exchange__row')][contains(.,'USD')]//*[@class='main-page-exchange__rate'])[2]"))
                .getText();
        return Double.parseDouble(usdSalaryS.replaceAll(",", "."));
    }

    public Double usdPurchaseDouble() {
        String usdPuschaseS = $(By.xpath("(//*[contains(@class,'main-page-exchange__row')][contains(.,'USD')]//*[@class='main-page-exchange__rate'])[1]"))
                .getText();
        return Double.parseDouble(usdPuschaseS.replaceAll(",", "."));
    }
}
